FROM node:12

WORKDIR/app

Copy package*.json ./

RUN npm ci

COPY . .

EXPOSE 3000

CMD["npm","start"]
